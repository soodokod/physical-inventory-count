<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class RecurringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        $validator = Validator::make($request->all(), [
            'from' => 'required|date',
            'to'   => 'required|date',
            'category'   => 'required',
            'department' => 'required',
        ]);

        $mscWarehouse = DB::table('mscWarehouse')
            ->select('PK_mscWarehouse', 'description')
            ->where('isMedicines', '=', 1)
            ->orWhere('isSupplies', '=', 1)
            ->orWhere('isExams', '=', 1)
            ->orWhere('isProcedures', '=', 1)
            ->orWhere('isAssets', '=', 1)
            ->orWhere('isOthers', '=', 1)
            ->get();

        $data['mscWarehouse'] = $mscWarehouse;

        $mscItemCategory = DB::table('mscItemCategory')
            ->select('PK_mscItemCategory', 'description')
            ->where('PK_mscItemCategory', '<>', 1000)
            ->get();

        $data['mscItemCategory'] = $mscItemCategory;
        
        if (!$validator->fails()) {
            
            $from = collect([$request->input('from'), '00:00:00'])->implode(' ');
            $to   = collect([$request->input('to'), '23:59:59'])->implode(' ');
            $category   = $request->input('category');
            $department = $request->input('department');

         /**
          * SELECT   convert(varchar(20), a.docdate, 107) as docdate, a.docno, b.FK_iwItems, 
                     c.itemdesc, count(b.FK_iwItems) as count
            FROM     iwPhentinv a, iwPhentitem b, iwItems c
            WHERE    b.FK_TRXNO = a.PK_TRXNO AND b.FK_iwItems = c.PK_iwItems
                     AND a.FK_mscWarehouse = 1033 AND c.FK_mscItemCategory = 1001
                     AND a.cancelflag = 0 AND a.deleteflag = 0 
                     AND a.docdate BETWEEN '2018-10-01 00:00:00' AND '2018-10-31 23:59:59'
            GROUP BY a.docdate, a.docno, b.FK_iwItems, c.itemdesc
            HAVING   count(b.FK_iwItems) > 1
          */

            $conditions = collect([
                ['a.FK_mscWarehouse', '=', $department],
                ['a.cancelflag', '=', 0],
                ['a.deleteflag', '=', 0],
            ]);

            if (is_numeric($category)) $conditions->push(['c.FK_mscItemCategory', '=', $category]);

            $items = DB::table('iwPhentinv as a')
                ->select(DB::raw("CONVERT(VARCHAR(20), a.docdate, 107) as docdate"), 'a.docno', 'b.FK_iwItems', 'c.itemdesc', DB::raw("count(b.FK_iwItems) as count"))
                ->join('iwPhentitem as b', 'b.FK_TRXNO', '=', 'a.PK_TRXNO')
                ->join('iwItems as c', 'b.FK_iwItems', '=', 'c.PK_iwItems')
                ->where($conditions->toArray())
                ->whereBetween('a.docdate', ["$from", "$to"])
                ->groupBy('a.docdate')
                ->groupBy('a.docno')
                ->groupBy('b.FK_iwItems')
                ->groupBy('c.itemdesc')
                ->havingRaw('count(b.FK_iwItems) > ?', [1])
                ->orderBy('a.docno', 'asc')
                ->orderBy('b.FK_iwItems', 'asc')
                ->paginate(15);
                
            // Retrieve all of the query string values 
            $query_string = $request->query();

            // Appending To Pagination Links: to append query string to each pagination link
            $items = $items->appends($query_string);

            $data['items'] = $items;
            
        }
        
        return view('recurring', $data);
    }

    public function download(Request $request)
    {
        $from = collect([$request->input('from'), '00:00:00'])->implode(' ');
        $to   = collect([$request->input('to'), '23:59:59'])->implode(' ');
        $category   = $request->input('category');
        $department = $request->input('department');

        $conditions = collect([
            ['a.FK_mscWarehouse', '=', $department],
            ['a.cancelflag', '=', 0],
            ['a.deleteflag', '=', 0],
        ]);

        if (is_numeric($category)) $conditions->push(['c.FK_mscItemCategory', '=', $category]);

        $items = DB::table('iwPhentinv as a')
                ->select(DB::raw("CONVERT(VARCHAR(20), a.docdate, 107) as docdate"), 'a.docno', 'b.FK_iwItems', 'c.itemdesc', DB::raw("count(b.FK_iwItems) as count"))
                ->join('iwPhentitem as b', 'b.FK_TRXNO', '=', 'a.PK_TRXNO')
                ->join('iwItems as c', 'b.FK_iwItems', '=', 'c.PK_iwItems')
                ->where($conditions->toArray())
                ->whereBetween('a.docdate', ["$from", "$to"])
                ->groupBy('a.docdate')
                ->groupBy('a.docno')
                ->groupBy('b.FK_iwItems')
                ->groupBy('c.itemdesc')
                ->havingRaw('count(b.FK_iwItems) > ?', [1])
                ->orderBy('a.docno', 'asc')
                ->orderBy('b.FK_iwItems', 'asc')
                ->get();

        // PhpSpreadsheet
        $spreadsheet = new Spreadsheet();

        // Setting a Font name and size
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');

        $spreadsheet->getDefaultStyle()->getFont()->setSize(12);

        $sheet = $spreadsheet->getActiveSheet();
        
        $row = 1;

        $column = 1;

        $headers = ['Document Date', 'Document No.', 'Item ID', 'Description', 'Count'];

        // Set bold Header
        $sheet->getStyle('1:1')->getFont()->setBold(true);

        // Freezing first line https://www.askingbox.com/question/phpexcel-freeze-first-line-and-column
        $sheet->freezePane('A2');

        for (; $column <= count($headers); $column++) { 
            
            $header = $headers[$column - 1];
            
            // Setting a cell value by column and row
            $sheet->setCellValueByColumnAndRow($column, $row, $header);

            // Setting a column's width
            $sheet->getColumnDimensionByColumn($column)->setAutoSize(true);

        }

        // Config Content
        foreach ($items as $key => $value) {

            ++$row;

            $item = collect($value);

            $keys = $item->keys();

            for ($column = 1; $column <= $item->count(); $column++) { 

                $key = $keys[$column - 1];

                $value = $item->get($key);

                $n = str_replace(',', '', $value);

                $datatype = is_numeric($n) ? DataType::TYPE_NUMERIC : DataType::TYPE_STRING;

                switch ($datatype) {
                        
                    case DataType::TYPE_NUMERIC:

                        $value = $n;
                        
                        break;

                    default: break;

                }

                // Retrieving a cell by column and row, and Explicitly set a cell's datatype and value
                $sheet->getCellByColumnAndRow($column, $row)->setValueExplicit($value, $datatype);

                switch ($key) {
                    case 'docno': 
                    case 'count': 
                        
                        $format = NumberFormat::FORMAT_NUMBER;

                        break;

                    default: break;
                }

                switch ($datatype) {

                    case DataType::TYPE_NUMERIC:

                        // Setting a Number Format
                        $sheet->getCellByColumnAndRow($column, $row)->getStyle()->getNumberFormat()->setFormatCode($format);
                        
                        break;

                    default: break;

                }

            }

        }

        $writer = new Xlsx($spreadsheet);

        $filename = 'Physical Count Inventory(Recurring).xlsx';

        $writer->save($filename);

        return redirect("/$filename");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
