<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class UncountedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        $validator = Validator::make($request->all(), [
            'from' => 'required|date',
            'to'   => 'required|date',
            'category'   => 'required',
            'department' => 'required',
        ]);

        $mscWarehouse = DB::table('mscWarehouse')
            ->select('PK_mscWarehouse', 'description')
            ->where('isMedicines', '=', 1)
            ->orWhere('isSupplies', '=', 1)
            ->orWhere('isExams', '=', 1)
            ->orWhere('isProcedures', '=', 1)
            ->orWhere('isAssets', '=', 1)
            ->orWhere('isOthers', '=', 1)
            ->get();

        $data['mscWarehouse'] = $mscWarehouse;

        $mscItemCategory = DB::table('mscItemCategory')
            ->select('PK_mscItemCategory', 'description')
            ->where('PK_mscItemCategory', '<>', 1000)
            ->get();

        $data['mscItemCategory'] = $mscItemCategory;
        
        if (!$validator->fails()) {
            
            $from = collect([$request->input('from'), '00:00:00'])->implode(' ');
            $to   = collect([$request->input('to'), '23:59:59'])->implode(' ');
            $category   = $request->input('category');
            $department = $request->input('department');
            
            /**
             * SELECT   a.FK_iwItems, b.itemdesc, SUM(a.qtyin) - SUM(a.qtyout) as qty
               FROM     iwItemLedger a, iwItems b
               WHERE    a.FK_mscWarehouse = 1033 AND a.FK_iwItems = b.PK_iwItems AND a.FK_iwItems IN (
                     --SELECT b.PK_iwItems, b.itemdesc
                     SELECT b.PK_iwItems
                     FROM   iwWareitem a, iwItems b
                     WHERE  a.FK_iwItems = b.PK_iwItems AND a.FK_mscWarehouse = 1033 
                            AND a.isTrade = 1 AND a.isViewable = 1 AND b.isactive = 1
                            AND b.FK_mscItemCategory = 1001 AND b.PK_iwItems NOT IN (
                                SELECT b.FK_iwItems
                                FROM   iwPhentinv a, iwPhentitem b, iwItems c
                                WHERE  b.FK_TRXNO = a.PK_TRXNO AND b.FK_iwItems = c.PK_iwItems
                                       AND a.FK_mscWarehouse = 1033 AND c.FK_mscItemCategory = 1001
                                       AND a.cancelflag = 0 AND a.deleteflag = 0
                                       AND a.docdate BETWEEN '2018-10-01 00:00:00' AND '2018-10-31 23:59:59'
                        )
                     )
               GROUP BY a.FK_iwItems, b.itemdesc
               HAVING   SUM(a.qtyin) - SUM(a.qtyout) > 0
             */

            $b_FK_mscItemCategory = is_numeric($category) ? "AND b.FK_mscItemCategory = $category" : "";

            $c_FK_mscItemCategory = is_numeric($category) ? "AND c.FK_mscItemCategory = $category" : "";

            $sql = "SELECT   a.FK_iwItems, b.itemdesc, FORMAT(SUM(a.qtyin) - SUM(a.qtyout), '#,##0.00') as qty
                    FROM     iwItemLedger a, iwItems b
                    WHERE    a.FK_mscWarehouse = $department AND a.FK_iwItems = b.PK_iwItems AND a.FK_iwItems IN (
                         SELECT b.PK_iwItems
                         FROM   iwWareitem a, iwItems b
                         WHERE  a.FK_iwItems = b.PK_iwItems AND a.FK_mscWarehouse = $department 
                                AND a.isTrade = 1 AND a.isViewable = 1 AND b.isactive = 1 
                                $b_FK_mscItemCategory AND b.PK_iwItems NOT IN (
                                    SELECT b.FK_iwItems
                                    FROM   iwPhentinv a, iwPhentitem b, iwItems c
                                    WHERE  b.FK_TRXNO = a.PK_TRXNO AND b.FK_iwItems = c.PK_iwItems
                                           AND a.FK_mscWarehouse = $department $c_FK_mscItemCategory
                                           AND a.cancelflag = 0 AND a.deleteflag = 0
                                           AND a.docdate BETWEEN '$from' AND '$to'
                            )
                         )
                    GROUP BY a.FK_iwItems, b.itemdesc
                    HAVING   SUM(a.qtyin) - SUM(a.qtyout) > 0";

            $items = DB::select($sql);

            // Manually Create a new paginator instance
            $perPage = 15;

            $currentPage = $request->input('page', 1);

            $offSet = ($currentPage * $perPage) - $perPage;  

            $total = count($items);

            $items = array_slice($items, $offSet, $perPage, true);  

            $pagination = new LengthAwarePaginator($items, $total, $perPage, $currentPage);

            $path = route('uncounted');

            // Set the base path to assign to all URLs
            $pagination->setPath($path);

            // Retrieve all of the query string values 
            $query_string = $request->query();

            // Appending To Pagination Links: to append query string to each pagination link
            $pagination = $pagination->appends($query_string);

            $data['items'] = $pagination;

        }
        
        return view('uncounted', $data);
    }

    public function download(Request $request)
    {
        $from = collect([$request->input('from'), '00:00:00'])->implode(' ');
        $to   = collect([$request->input('to'), '23:59:59'])->implode(' ');
        $category   = $request->input('category');
        $department = $request->input('department');
        
        $b_FK_mscItemCategory = is_numeric($category) ? "AND b.FK_mscItemCategory = $category" : "";

        $c_FK_mscItemCategory = is_numeric($category) ? "AND c.FK_mscItemCategory = $category" : "";

        $sql = "SELECT   a.FK_iwItems, b.itemdesc, SUM(a.qtyin) - SUM(a.qtyout) as qty
                FROM     iwItemLedger a, iwItems b
                WHERE    a.FK_mscWarehouse = $department AND a.FK_iwItems = b.PK_iwItems AND a.FK_iwItems IN (
                     SELECT b.PK_iwItems
                     FROM   iwWareitem a, iwItems b
                     WHERE  a.FK_iwItems = b.PK_iwItems AND a.FK_mscWarehouse = $department 
                            AND a.isTrade = 1 AND a.isViewable = 1 AND b.isactive = 1 
                            $b_FK_mscItemCategory AND b.PK_iwItems NOT IN (
                                SELECT b.FK_iwItems
                                FROM   iwPhentinv a, iwPhentitem b, iwItems c
                                WHERE  b.FK_TRXNO = a.PK_TRXNO AND b.FK_iwItems = c.PK_iwItems
                                       AND a.FK_mscWarehouse = $department $c_FK_mscItemCategory
                                       AND a.cancelflag = 0 AND a.deleteflag = 0
                                       AND a.docdate BETWEEN '$from' AND '$to'
                        )
                     )
                GROUP BY a.FK_iwItems, b.itemdesc
                HAVING   SUM(a.qtyin) - SUM(a.qtyout) > 0";

        $items = DB::select($sql);

        // PhpSpreadsheet
        $spreadsheet = new Spreadsheet();

        // Setting a Font name and size
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');

        $spreadsheet->getDefaultStyle()->getFont()->setSize(12);

        $sheet = $spreadsheet->getActiveSheet();
        
        $row = 1;

        $column = 1;

        $headers = ['Item ID', 'Description', 'Qty'];

        // Set bold Header
        $sheet->getStyle('1:1')->getFont()->setBold(true);

        // Freezing first line https://www.askingbox.com/question/phpexcel-freeze-first-line-and-column
        $sheet->freezePane('A2');

        for (; $column <= count($headers); $column++) { 
            
            $header = $headers[$column - 1];
            
            // Setting a cell value by column and row
            $sheet->setCellValueByColumnAndRow($column, $row, $header);

            // Setting a column's width
            $sheet->getColumnDimensionByColumn($column)->setAutoSize(true);

        }

        // Config Content
        foreach ($items as $key => $value) {

            ++$row;

            $item = collect($value);

            $keys = $item->keys();

            for ($column = 1; $column <= $item->count(); $column++) { 

                $key = $keys[$column - 1];

                $value = $item->get($key);

                $n = str_replace(',', '', $value);

                $datatype = is_numeric($n) ? DataType::TYPE_NUMERIC : DataType::TYPE_STRING;

                switch ($datatype) {
                        
                    case DataType::TYPE_NUMERIC:

                        $value = $n;
                        
                        break;

                    default: break;

                }

                // Retrieving a cell by column and row, and Explicitly set a cell's datatype and value
                $sheet->getCellByColumnAndRow($column, $row)->setValueExplicit($value, $datatype);

                switch ($key) {
                    case 'qty': 
                        
                        $format = NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1;

                        break;

                    default: break;
                }

                switch ($datatype) {

                    case DataType::TYPE_NUMERIC:

                        // Setting a Number Format
                        $sheet->getCellByColumnAndRow($column, $row)->getStyle()->getNumberFormat()->setFormatCode($format);
                        
                        break;

                    default: break;

                }

            }

        }

        $writer = new Xlsx($spreadsheet);

        $filename = 'Physical Count Inventory(Uncounted).xlsx';

        $writer->save($filename);

        return redirect("/$filename");
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
