<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        $validator = Validator::make($request->all(), [
            'from' => 'required|date',
            'to'   => 'required|date',
            'department' => 'required',
        ]);

        $mscWarehouse = DB::table('mscWarehouse')
            ->select('PK_mscWarehouse', 'description')
            ->where('isMedicines', '=', 1)
            ->orWhere('isSupplies', '=', 1)
            ->orWhere('isExams', '=', 1)
            ->orWhere('isProcedures', '=', 1)
            ->orWhere('isAssets', '=', 1)
            ->orWhere('isOthers', '=', 1)
            ->get();

        $data['mscWarehouse'] = $mscWarehouse;

        if (!$validator->fails()) {
            
            $from = collect([$request->input('from'), '00:00:00'])->implode(' ');
            $to   = collect([$request->input('to'), '23:59:59'])->implode(' ');
            $department = $request->input('department');

         /**
          * SELECT    a.docno, convert(varchar(20), a.docdate, 100) as docdate,
                      b.fullname as ASUPost, a.countedby, CONVERT(DECIMAL(10,2), a.totqty) as totqty,
                      a.totitm, a.postingtype, a.remarks, a.postflag, a.postdate, a.cancelflag, a.canceldate
            FROM      iwPhentinv a
            LEFT JOIN psDatacenter b ON a.FK_ASUPost = b.PK_psDatacenter
            WHERE     a.docdate BETWEEN '2017-05-27 00:00:00' AND '2017-06-01 23:59:59'
                      AND a.FK_mscWarehouse = 1033
            ORDER BY  a.docno
          */

            $documents = DB::table('iwPhentinv as a')
                ->select('a.docno', DB::raw("convert(varchar(20), a.docdate, 107) as docdate"), 'b.fullname as ASUPost', 'a.countedby', DB::raw("FORMAT(a.totqty, '#,##0.00') as totqty"), DB::raw("FORMAT(a.totitm, '#,##0.00') as totitm"), 'a.postingtype', 'a.remarks', 'a.postflag', DB::raw("convert(varchar(20), a.postdate, 100) as postdate"), 'a.cancelflag', DB::raw("convert(varchar(20), a.canceldate, 100) as canceldate"))
                ->leftJoin('psDatacenter as b', 'a.FK_ASUPost', '=', 'b.PK_psDatacenter')
                ->whereBetween('a.docdate', ["$from", "$to"])
                ->where([
                    ['a.FK_mscWarehouse', '=', $department],
                ])
                ->orderBy('a.docno', 'asc')
                ->paginate(15);
                
            // Retrieve all of the query string values 
            $query_string = $request->query();

            // Appending To Pagination Links: to append query string to each pagination link
            $documents = $documents->appends($query_string);

            $data['documents'] = $documents;

        }

        return view('document', $data);
    }

    public function download(Request $request)
    {
        $from = collect([$request->input('from'), '00:00:00'])->implode(' ');
        $to   = collect([$request->input('to'), '23:59:59'])->implode(' ');
        $department = $request->input('department');

        $documents = DB::table('iwPhentinv as a')
            ->select('a.docno', DB::raw("convert(varchar(20), a.docdate, 107) as docdate"), 'b.fullname as ASUPost', 'a.countedby', DB::raw("FORMAT(a.totqty, '#,##0.00') as totqty"), DB::raw("FORMAT(a.totitm, '#,##0.00') as totitm"), 'a.postingtype', 'a.remarks', DB::raw("convert(varchar(20), a.postdate, 100) as postdate"), DB::raw("convert(varchar(20), a.canceldate, 100) as canceldate"))
            ->leftJoin('psDatacenter as b', 'a.FK_ASUPost', '=', 'b.PK_psDatacenter')
            ->whereBetween('a.docdate', ["$from", "$to"])
            ->where([
                ['a.FK_mscWarehouse', '=', $department],
            ])
            ->orderBy('a.docno', 'asc')
            ->get(15);

        // PhpSpreadsheet
        $spreadsheet = new Spreadsheet();

        // Setting a Font name and size
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');

        $spreadsheet->getDefaultStyle()->getFont()->setSize(12);

        $sheet = $spreadsheet->getActiveSheet();
        
        $row = 1;

        $column = 1;

        $headers = ['Document No.', 'Document Date', 'Encoded By', 'Counted By', 'Total Qty', 'Total Item', 'Posting Type', 'Remarks', 'Posted Date', 'Cancelled Date'];

        // Set bold Header
        $sheet->getStyle('1:1')->getFont()->setBold(true);

        // Freezing first line https://www.askingbox.com/question/phpexcel-freeze-first-line-and-column
        $sheet->freezePane('A2');

        for (; $column <= count($headers); $column++) { 
            
            $header = $headers[$column - 1];
            
            // Setting a cell value by column and row
            $sheet->setCellValueByColumnAndRow($column, $row, $header);

            // Setting a column's width
            $sheet->getColumnDimensionByColumn($column)->setAutoSize(true);

        }

        // Config Content
        foreach ($documents as $key => $value) {

            ++$row;

            $item = collect($value);

            $keys = $item->keys();

            for ($column = 1; $column <= $item->count(); $column++) { 

                $key = $keys[$column - 1];

                $value = $item->get($key);

                $n = str_replace(',', '', $value);

                $datatype = is_numeric($n) ? DataType::TYPE_NUMERIC : DataType::TYPE_STRING;

                switch ($datatype) {
                        
                    case DataType::TYPE_NUMERIC:

                        $value = $n;
                        
                        break;

                    default: break;

                }

                // Retrieving a cell by column and row, and Explicitly set a cell's datatype and value
                $sheet->getCellByColumnAndRow($column, $row)->setValueExplicit($value, $datatype);

                switch ($key) {
                    case 'docno': 
                        
                        $format = NumberFormat::FORMAT_NUMBER;

                        break;

                    case 'totqty': 
                    case 'totitm':

                        $format = NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1;

                        break;

                    default: break;
                }

                switch ($datatype) {

                    case DataType::TYPE_NUMERIC:

                        // Setting a Number Format
                        $sheet->getCellByColumnAndRow($column, $row)->getStyle()->getNumberFormat()->setFormatCode($format);
                        
                        break;

                    default: break;

                }

            }

        }

        $writer = new Xlsx($spreadsheet);

        $filename = 'Physical Count Inventory(Documents).xlsx';

        $writer->save($filename);

        return redirect("/$filename");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
