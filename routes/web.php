<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect()->route('document'); });

Route::get('/document', 'DocumentController@index')->name('document');
Route::get('/document/download', 'DocumentController@download')->name('document.download');

Route::get('/detail', 'DetailController@index')->name('detail');
Route::get('/detail/download', 'DetailController@download')->name('detail.download');

Route::get('/recurring', 'RecurringController@index')->name('recurring');
Route::get('/recurring/download', 'RecurringController@download')->name('recurring.download');

Route::get('/uncounted', 'UncountedController@index')->name('uncounted');
Route::get('/uncounted/download', 'UncountedController@download')->name('uncounted.download');