@extends('layouts.app')

@section('content')
<div class="tab-content">
    <div class="tab-pane fade show active" role="tabpanel">
	    @php
	    	$args = [
	    		'from' => Request::input('from'), 
	    		'to'   => Request::input('to'), 
	    		'category'   => Request::input('category'), 
	    		'department' => Request::input('department')
	    	];
	    @endphp

    	@isset($items)
	        @php $rowno = $items->firstItem(); @endphp
	        
	        @if ($items->isNotEmpty())
	        <div class="bd-example my-3">
	            <p class="mb-0">Showing {{ $rowno }} to {{ $items->lastItem() }} of {{ $items->total() }} entries <a href="{{ route('detail.download', $args) }}" class="float-right">Download All</a></p>
	        </div>
	        @endif
	    @endisset
    	
    	<table class="table table-hover table-responsive-md">
	        <thead>
	            <tr>
	                <th scope="col" class="border-top-0">&#9608;</th>
	                <th scope="col" class="border-top-0">Document Date</th>
	                <th scope="col" class="border-top-0">Document No.</th>
	                <th scope="col" class="border-top-0">Item ID</th>
	                <th scope="col" class="border-top-0">Description</th>
	                <th scope="col" class="border-top-0">Entry Date</th>
	                <th scope="col" class="border-top-0">Physical Count Qty</th>
	                <th scope="col" class="border-top-0">Unit</th>
	                <th scope="col" class="border-top-0">Conversion</th>
	                <th scope="col" class="border-top-0">Total Amount</th>
	                <th scope="col" class="border-top-0">System Count Qty</th>
	                <th scope="col" class="border-top-0">Variance Qty</th>
	                <th scope="col" class="border-top-0">Counted By</th>
	            </tr>
	        </thead>
	        <tbody>
	        	@isset($items)
	                @if ($items->isEmpty()) <tr> <td colspan="13">No results found</td> </tr> @endif

		            @foreach ($items as $item)
		            <tr>
		                <th scope="row">{{ $rowno++ }}</th>
		                <td>{{ $item->docdate }}</td>
		                <td>{{ $item->docno }}</td>
		                <td>{{ $item->FK_iwItems }}</td>
		                <td>{{ strlen($item->itemdesc) > 15 ? substr($item->itemdesc, 0, 15) . '...' : $item->itemdesc }}</td>
		                <td>{{ $item->entrydate }}</td>
		                <td>{{ $item->qty }}</td>
		                <td>{{ $item->unit }}</td>
		                <td>{{ $item->conversion }}</td>
		                <td>{{ $item->netcost }}</td>
		                <td>{{ $item->Sysqty }}</td>
		                <td>{{ $item->varianceqty }}</td>
		                <td>{{ strlen($item->countedby) > 15 ? substr($item->countedby, 0, 15) . '...' : $item->countedby }}</td>
		            </tr>
		            @endforeach
		        @endisset
	        </tbody>
	    </table>

	    @isset($items)
	    <nav aria-label="Page navigation example">
	        @php
	            $current_item = $items->currentPage();
	            
	            $length = $items->lastPage();
	            
	            $width = 5; //length of page item
	            
	            $center = 3; // center of $width
	            
	            $last_item = $width;

	            if ($length > $width) {

	                $a = $current_item - $center;

	                if ($a > -1) {

	                    $last_item = $width + $a;

	                    $first_item = $a + 1;

	                } else {
	                    
	                    $first_item = 1;

	                }

	                if ($last_item > $length) {

	                    $last_item = $length;

	                    $first_item = ($length - $width) + 1;

	                }

	            } else {

	                $first_item = 1;

	                $last_item = $length;

	            }
	        @endphp

	        @if ($items->isNotEmpty())
	        <ul class="pagination justify-content-center">
	            <li class="page-item @if (!$items->previousPageUrl()) disabled @endif">
	                <a class="page-link" href="{{ $items->previousPageUrl() }}" tabindex="-1">Previous</a>
	            </li>

	            @for ($i = $first_item; $i <= $last_item; $i++)

	            	@php $args['page'] = $i; @endphp

	            	<li class="page-item @if ($current_item === $i) active @endif">
	                    <a class="page-link" href="{{ route(Request::path(), $args) }}">{{ $i }}</a>
	                </li>
	            @endfor
	            <li class="page-item @if (!$items->nextPageUrl()) disabled @endif">
	                <a class="page-link" href="{{ $items->nextPageUrl() }}">Next</a>
	            </li>
	        </ul>
	        @endif
	    </nav>
	    @endisset
    </div>
</div>
@endsection