@extends('layouts.app')

@section('content')
<div class="tab-content">
    <div class="tab-pane fade show active" role="tabpanel">
	    @php
	    	$args = [
	    		'from' => Request::input('from'), 
	    		'to'   => Request::input('to'), 
	    		'department' => Request::input('department')
	    	];
	    @endphp

    	@isset($documents)
	        @php $rowno = $documents->firstItem(); @endphp
	        
	        @if ($documents->isNotEmpty())
	        <div class="bd-example my-3">
	            <p class="mb-0">Showing {{ $rowno }} to {{ $documents->lastItem() }} of {{ $documents->total() }} entries <a href="{{ route('document.download', $args) }}" class="float-right">Download All</a></p>
	        </div>
	        @endif
	    @endisset
    	
    	<table class="table table-hover table-responsive-md">
	        <thead>
	            <tr>
	                <th scope="col" class="border-top-0">&#9608;</th>
	                <th scope="col" class="border-top-0">Document No.</th>
	                <th scope="col" class="border-top-0">Document Date</th>	                
	                <th scope="col" class="border-top-0">Encoded By</th>
	                <th scope="col" class="border-top-0">Counted By</th>
	                <th scope="col" class="border-top-0">Total Qty</th>
	                <th scope="col" class="border-top-0">Total Item</th>
	                <th scope="col" class="border-top-0">Posting Type</th>
	                <th scope="col" class="border-top-0">Remarks</th>
	                <th scope="col" class="border-top-0">Posted Date</th>
	                <th scope="col" class="border-top-0">Cancelled Date</th>
	            </tr>
	        </thead>
	        <tbody>
	        	@isset($documents)
	                @if ($documents->isEmpty()) <tr> <td colspan="13">No results found</td> </tr> @endif

		            @foreach ($documents as $document)
		            <tr>
		                <th scope="row">{{ $rowno++ }}</th>
		                <td>{{ $document->docno }}</td>
		                <td>{{ $document->docdate }}</td>
		                <td>{{ $document->ASUPost }}</td>
		                <td>{{ $document->countedby }}</td>
		                <td>{{ $document->totqty }}</td>
		                <td>{{ $document->totitm }}</td>
		                <td>{{ $document->postingtype }}</td>
		                <td>{{ $document->remarks }}</td>
		                <td>{{ $document->postdate }}</td>
		                <td>{{ $document->canceldate }}</td>
		            </tr>
		            @endforeach
		        @endisset
	        </tbody>
	    </table>

	    @isset($documents)
	    <nav aria-label="Page navigation example">
	        @php
	            $current_item = $documents->currentPage();
	            
	            $length = $documents->lastPage();
	            
	            $width = 5; //length of page item
	            
	            $center = 3; // center of $width
	            
	            $last_item = $width;

	            if ($length > $width) {

	                $a = $current_item - $center;

	                if ($a > -1) {

	                    $last_item = $width + $a;

	                    $first_item = $a + 1;

	                } else {
	                    
	                    $first_item = 1;

	                }

	                if ($last_item > $length) {

	                    $last_item = $length;

	                    $first_item = ($length - $width) + 1;

	                }

	            } else {

	                $first_item = 1;

	                $last_item = $length;

	            }
	        @endphp

	        @if ($documents->isNotEmpty())
	        <ul class="pagination justify-content-center">
	            <li class="page-item @if (!$documents->previousPageUrl()) disabled @endif">
	                <a class="page-link" href="{{ $documents->previousPageUrl() }}" tabindex="-1">Previous</a>
	            </li>

	            @for ($i = $first_item; $i <= $last_item; $i++)

	            	@php $args['page'] = $i; @endphp

	            	<li class="page-item @if ($current_item === $i) active @endif">
	                    <a class="page-link" href="{{ route(Request::path(), $args) }}">{{ $i }}</a>
	                </li>
	            @endfor
	            <li class="page-item @if (!$documents->nextPageUrl()) disabled @endif">
	                <a class="page-link" href="{{ $documents->nextPageUrl() }}">Next</a>
	            </li>
	        </ul>
	        @endif
	    </nav>
	    @endisset
    </div>
</div>
@endsection