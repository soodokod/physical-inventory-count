<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('library/bootstrap/4.1.3/css/bootstrap.min.css') }}">

        <title>{{ config('app.name') }}</title>

        <link rel="stylesheet" href="{{ asset('library/app.css') }}">        
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark border-bottom">
            <a class="navbar-brand" href="javascript:void(0)">{{ config('app.name') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#filters" aria-controls="filters" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="filters">
                <form class="form-inline my-2 my-lg-0" action="{{ route(Request::path()) }}" method="GET">
                    <div class="input-group mr-sm-2 mb-2 mb-sm-0">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="from">From</span>
                        </div>
                        <input type="date" class="form-control" name="from" value="{{ Request::input('from') }}" placeholder="From" aria-label="From" aria-describedby="from">
                    </div>
                    <div class="input-group mr-sm-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="to">To</span>
                        </div>
                        <input type="date" class="form-control" name="to" value="{{ Request::input('to') }}" placeholder="To" aria-label="To" aria-describedby="to">
                    </div>
                    @if (Request::path() !== "document")
                    <select class="form-control mr-sm-2 mt-2 mt-sm-0" name="category">
                        <option selected disabled hidden>Select Item Category</option>
                        <option value="all" @if (Request::input('category') === "all") selected @endif>All Item Catetory</option>
                        @foreach ($mscItemCategory as $category)
                        <option value="{{ $category->PK_mscItemCategory }}" @if (Request::input('category') === $category->PK_mscItemCategory) selected @endif>{{ $category->description }}</option>
                        @endforeach
                    </select>
                    @endif
                    <select class="form-control mr-sm-2 mt-2 mt-sm-0" name="department">
                        <option selected disabled hidden>Select Department</option>
                        @foreach ($mscWarehouse as $warehouse)
                        <option value="{{ $warehouse->PK_mscWarehouse }}" @if (Request::input('department') === $warehouse->PK_mscWarehouse) selected @endif>{{ $warehouse->description }}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Apply Filters</button>
                </form>
            </div>
        </nav>

        <div class="container-fluid pt-3">
            @php
                $args = [];
                
                if (!empty(Request::all())) {

                    $args = [
                        'from' => Request::input('from'), 
                        'to'   => Request::input('to'), 
                        'category'   => Request::input('category'), 
                        'department' => Request::input('department')
                    ];

                }
            @endphp

            <nav>
                <div class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link @if (Request::is('document')) active @endif" href="{{ route('document', $args) }}">Documents</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (Request::is('detail')) active @endif" href="{{ route('detail', $args) }}">Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (Request::is('recurring')) active @endif" href="{{ route('recurring', $args) }}">Recurring</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (Request::is('uncounted')) active @endif" href="{{ route('uncounted', $args) }}">Uncounted</a>
                    </li>
                </div>
            </nav>
            @yield('content')
        </div>
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ asset('library/jquery/3.3.1/jquery.slim.min.js') }}"></script>
        <script src="{{ asset('library/popper/1.14.3/popper.min.js') }}"></script>
        <script src="{{ asset('library/bootstrap/4.1.3/js/bootstrap.min.js') }}"></script>
    </body>
</html>